<?php
/**
 * Created by PhpStorm.
 * User: jhoal
 * Date: 18/09/2017
 * Time: 19:26
 */

namespace App\Traits;

use Ramsey\Uuid\Uuid;

trait Uuids
{
    /**
     * Boot the Uuid trait for the model.
     *
     * @return void
     */
    public static function boot()
    {
        static::creating(function ($model) {
            $model->incrementing = false;
            $model->{$model->getKeyName()} = (string)Uuid::uuid4();
        });
    }

}